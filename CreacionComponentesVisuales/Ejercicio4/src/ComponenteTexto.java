
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class ComponenteTexto extends JTextField implements Serializable{
    private boolean numeric;

    public ComponenteTexto() {
        numeric = false;
        setText("");
        
        this.addKeyListener(new KeyListener(){
            String actual = "";
            boolean matches = false;
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == 8) return;
                if (numeric){
                    char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7',
                            '8', '9'};
                    for (char c : numbers){
                        if (c == e.getKeyChar()){
                            matches = true;
                            break;
                        }
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if(e.getKeyCode() == 8) return;
                if(numeric){
                    if(!matches){
                        setText(actual);
                    }
                    else{
                        matches = false;
                        actual = getText();
                    }
                }
            }
        });
    }

    public boolean isNumeric() {
        return numeric;
    }

    public void setNumeric(boolean numeric) {
        this.numeric = numeric;
    }
}
