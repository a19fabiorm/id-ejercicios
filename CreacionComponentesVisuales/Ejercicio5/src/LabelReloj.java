
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JLabel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class LabelReloj extends JLabel implements Serializable{
    boolean format12 = false;

    public LabelReloj() {
    }

    public boolean isFormat12() {
        return format12;
    }

    public void setFormat12(boolean format12) {
        this.format12 = format12;
    }
    
    public void showHour(){
        Runnable runnable = () -> {
            while(true){
                Calendar calendar = Calendar.getInstance();
                calendar = new GregorianCalendar();
                setText((format12? calendar.get(Calendar.HOUR) : 
                        calendar.get(Calendar.HOUR_OF_DAY)) + ":" + 
                        calendar.get(Calendar.MINUTE));
                
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            }
        };
        
        Thread hilo = new Thread(runnable);
        hilo.setDaemon(true);
        hilo.start();
    }
}
