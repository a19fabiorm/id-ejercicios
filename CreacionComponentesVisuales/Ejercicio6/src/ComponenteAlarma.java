
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class ComponenteAlarma extends JLabel implements Serializable{
    boolean format12 = false;
    Hora hora;
    boolean alarma = false;

    public ComponenteAlarma() {
    }

    public boolean isFormat12() {
        return format12;
    }

    public void setFormat12(boolean format12) {
        this.format12 = format12;
    }

    public Hora getHora() {
        return hora;
    }

    public void setHora(Hora hora) {
        this.hora = hora;
    }
    
    public void showHour(){
        Runnable runnable = () -> {
            while(true){
                Calendar calendar = Calendar.getInstance();
                calendar = new GregorianCalendar();
                setText((format12? calendar.get(Calendar.HOUR) : 
                        calendar.get(Calendar.HOUR_OF_DAY)) + ":" + 
                        calendar.get(Calendar.MINUTE));
                
                if(alarma && hora != null){
                    if(calendar.get(Calendar.HOUR_OF_DAY) == Integer.parseInt(hora.getHora()) &&
                        calendar.get(Calendar.MINUTE) == Integer.parseInt(hora.getMinuto())){
                        JOptionPane.showMessageDialog(this, "Alarma");
                    } 
                }
                
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            }
        };
        
        Thread hilo = new Thread(runnable);
        hilo.setDaemon(true);
        hilo.start();
    }
    
    public void setAlarma(boolean bool){
        alarma = bool;
    }
}
