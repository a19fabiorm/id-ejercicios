
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Componente extends JButton implements Serializable{
    private Colores colorHover;
    private Colores color;
    private ColorHoverListener listener;
    
    public Componente() {
        color = new Colores(Color.WHITE, Color.BLACK);
        changeColors(color);
        
        this.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseEntered(MouseEvent e){
                if (listener != null && listener.changeColorWhenHover() &&
                        colorHover != null){
                        
                    changeColors(colorHover);
                }
            }
            
            @Override
            public void mouseExited(MouseEvent e){
                if (listener != null && listener.changeColorWhenHover() && 
                        color != null){
                
                    changeColors(color);
                }
            }
        });
    }

    public Colores getColorHover() {
        return colorHover;
    }

    public void setColorHover(Colores colorHover) {
        this.colorHover = colorHover;
    }
    
    public void addColorHoverListener(ColorHoverListener listener){
        this.listener = listener;
    }
    
    public void removeColorHoverListener(){
        this.listener = null;
    }

    public Colores getColor() {
        return color;
    }

    public void setColor(Colores color) {
        this.color = color;
    }
    
    public void changeColors(Colores colors){
        if(colors.getColorFondo() != null)
            setBackground(colors.getColorFondo());
        if(colors.getColorTexto() != null)
            setForeground(colors.getColorTexto());
    }
}
