
import java.io.Serializable;
import javax.swing.JButton;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Componente extends JButton implements Serializable{
    private Colores color;

    public Componente() {
    }

    public Colores getColor() {
        return color;
    }

    public void setColor(Colores color) {
        setBackground(color.getColorFondo());
        setForeground(color.getColorTexto());
    }
}
