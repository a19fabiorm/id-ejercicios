/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.validation.api.builtin.stringvalidation;

import org.netbeans.validation.api.Problems;
import org.openide.util.NbBundle;

/**
 *
 * @author a19danielvt
 */
public class ValidacionMayusculas extends StringValidator{

    @Override
    public void validate(Problems problemas, String componente, String texto) {
        if(!texto.equals("") && !Character.isUpperCase(texto.charAt(0))){
            String mensaje = NbBundle.getMessage(this.getClass(), "MSG_MAYUSCULAS", componente);
            problemas.add(mensaje);
        }
    }
    
}
