
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;

public class Controller {

    @FXML
    private Button salir;

    public void initialize() {
        salir.setText("Pul_sa");
        salir.setMnemonicParsing(true);
        Tooltip tooltip = new Tooltip();
        tooltip.setText("Pulsa para salir de la aplicación");
        salir.setTooltip(tooltip);
    }
    
    public void salir(){
        Platform.exit();
    }
}
