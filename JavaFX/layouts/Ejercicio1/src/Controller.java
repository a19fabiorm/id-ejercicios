
import javafx.fxml.FXML;
import javafx.scene.control.Label;


public class Controller {
    
    @FXML
    private Label info;
    
    public void initialize(){
        info.setText("");
    }
    
    public void action(){
        info.setText("Botón pulsado");
    }
}
