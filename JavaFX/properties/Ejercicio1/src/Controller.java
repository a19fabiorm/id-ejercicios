
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;


public class Controller {
    
    @FXML
    private CheckBox check;
    
    @FXML
    public void initialize(){
    }
    
    public void titulo(){
        if(check.isSelected()){
            ((Stage)(check.getScene().getWindow())).setTitle("hola");
        }else{
            ((Stage)(check.getScene().getWindow())).setTitle("");
        }
    }
}
