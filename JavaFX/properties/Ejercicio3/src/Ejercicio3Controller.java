/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio3Controller implements Initializable {

    @FXML
    private ChoiceBox<String> choice;
    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String[] valores = {"Uno", "Dos", "Tres"};
        choice.setItems(FXCollections.observableArrayList("Uno", "Dos", "Tres"));
        
        choice.getSelectionModel().selectedIndexProperty().addListener((ov, t, t1) -> {
            label.setText(valores[t1.intValue()]);
        });
    }    
    
}
