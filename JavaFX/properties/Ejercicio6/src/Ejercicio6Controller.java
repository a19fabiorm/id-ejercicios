/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio6Controller implements Initializable {

    @FXML
    private ToggleGroup botones;
    @FXML
    private Rectangle rectangulo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cambiarColor("#FFFFFF");
        
        botones.selectedToggleProperty().addListener((o) -> {
            if(((ToggleButton)botones.getSelectedToggle()) == null) return;
            
            switch(((ToggleButton)botones.getSelectedToggle()).getText()){
                case "Rojo":
                    cambiarColor("#FF0000");
                    break;
                case "Azul":
                    cambiarColor("#0000FF");
                    break;
                case "Verde":
                    cambiarColor("#00FF00");
                    break;
                default:
                    cambiarColor("#000000");
            }
        });
    }    
    
    public void cambiarColor(String color){
        rectangulo.setFill(Paint.valueOf(color));
    }
    
}
