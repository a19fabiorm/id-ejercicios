
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ipman
 */
public class Bill {
    private DoubleProperty amountDue = new SimpleDoubleProperty();

    public final double getAmountDue(){
        return amountDue.get();
    }
    
    public final void setAmountDue(double value){
        amountDue.setValue(value);
    }
    
    public DoubleProperty amountDueProperty(){
        return amountDue;
    }
}
