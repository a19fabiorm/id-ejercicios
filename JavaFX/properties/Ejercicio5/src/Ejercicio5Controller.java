/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio5Controller implements Initializable {

    @FXML
    private ToggleGroup botones;
    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        botones.selectedToggleProperty().addListener((o) -> {
            label.setText(((RadioButton)botones.getSelectedToggle()).getText());
        });
    }    
    
}
