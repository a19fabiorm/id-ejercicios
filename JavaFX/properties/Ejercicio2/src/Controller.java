
import java.io.InputStream;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Controller {
    
    @FXML
    private Slider slider;
    
    @FXML
    private ImageView image;
    
    public void initialize(){
        setImage();
        
        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                setImage();
            }
        });
    }
    
    public void setImage(){
        InputStream in = getClass().getResourceAsStream("images/volumen_100.png");
        int valor = (int) slider.getValue();
        if(valor == 0){
            in = getClass().getResourceAsStream("images/volumen_0.png");
        }else if(valor <= 33){
            in = getClass().getResourceAsStream("images/volumen_33.png");
        }else if(valor <= 66){
            in = getClass().getResourceAsStream("images/volumen_66.png");
        }

        image.setImage(new Image(in));
    }
}