/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class TablaController implements Initializable {

    @FXML
    private TableView<Person> table;
    
    ObservableList<Person> data = FXCollections.observableArrayList(
            new Person("nombre1", "apellidos1", "email1"),
            new Person("nombre2", "apellidos2", "email2"),
            new Person("nombre3", "apellidos3", "email3"),
            new Person("nombre4", "apellidos4", "email4"),
            new Person("nombre5", "apellidos5", "email5")
    );
    
    @FXML
    private TableColumn<Person, String> nombre;
    @FXML
    private TableColumn<Person, String> apellidos;
    @FXML
    private TableColumn<Person, String> email;
    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField mail;
    @FXML
    private Button add;
    /**
     * Initializes the controller class.
     */    
    public void initialize(URL url, ResourceBundle rb) {
        inicializarTabla();
        
        add.addEventFilter(MouseEvent.MOUSE_CLICKED, (t) -> {
            String nombre = name.getText();
            String apellidos = surname.getText();
            String email = mail.getText();
            
            if(nombre.matches("[ ]*") || email.matches("[ ]*") || apellidos.matches("[ ]*")){
                Alert alert = new Alert(AlertType.WARNING, "Cubre todos los campos");
                alert.show();
                return;
            }
            
            Person person = new Person(nombre, apellidos, email);
            data.add(person);
            
            inicializarFormulario();
        });
    }
    
    public void inicializarTabla(){
        nombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        nombre.setCellFactory(TextFieldTableCell.forTableColumn());
        nombre.setOnEditCommit((t) -> {
            t.getRowValue().setNombre(t.getNewValue());
            System.out.println(t.getRowValue());
        });
        
        apellidos.setCellValueFactory(new PropertyValueFactory<>("apellidos"));
        apellidos.setCellFactory(TextFieldTableCell.forTableColumn());
        apellidos.setOnEditCommit((t) -> {
            t.getRowValue().setApellidos(t.getNewValue());
            System.out.println(t.getRowValue());
        });
        
        email.setCellValueFactory(new PropertyValueFactory<>("email"));
        email.setCellFactory(TextFieldTableCell.forTableColumn());
        email.setOnEditCommit((t) -> {
            t.getRowValue().setEmail(t.getNewValue());
            System.out.println(t.getRowValue());
        });
        
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.setItems(data);
    }
    
    public void inicializarFormulario(){
        name.setText("");
        surname.setText("");
        mail.setText("");
    }
}
