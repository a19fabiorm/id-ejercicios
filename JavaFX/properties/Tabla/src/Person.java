
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author a19danielvt
 */
public class Person {
    private StringProperty nombre;
    private StringProperty apellidos;
    private StringProperty email;
    
    public Person(String nombre, String apellidos, String email){
        setNombre(nombre);
        setApellidos(apellidos);
        setEmail(email);
    }
    
    // NOMBRE
    public String getNombre() {
        return nombreProperty().get();
    }

    public void setNombre(String nombre) {
        nombreProperty().set(nombre);
    }
    
    public StringProperty nombreProperty(){
        if(nombre == null)
            nombre = new SimpleStringProperty(this, "nombre");
        
        return nombre;
    }
    
    // APELLIDOS
    public String getApellidos() {
        return apellidosProperty().get();
    }

    public void setApellidos(String apellidos) {
        apellidosProperty().set(apellidos);
    }
    
    public StringProperty apellidosProperty(){
        if(apellidos == null)
            apellidos = new SimpleStringProperty(this, "apellidos");
        
        return apellidos;
    }

    // EMAIL
    public String getEmail() {
        return emailProperty().get();
    }

    public void setEmail(String email) {
        emailProperty().set(email);
    }
    
    public StringProperty emailProperty(){
        if(email == null)
            email = new SimpleStringProperty(this, "email");
        
        return email;
    }
    
    public String toString(){
        return getNombre() + " " + getApellidos() + "\nMail:" + getEmail() + "\n";
    }
}
