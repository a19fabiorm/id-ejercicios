/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;

/**
 * FXML Controller class
 *
 * @author a19danielvt
 */
public class Ejercicio4Controller implements Initializable {

    @FXML
    private Button mas;
    @FXML
    private Button menos;
    @FXML
    private ProgressBar bar;
    @FXML
    private ProgressIndicator indicator;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void increase(ActionEvent event) {
        bar.setProgress(bar.getProgress() + 0.1);
        actualizarIndicator();
    }

    @FXML
    private void decrease(ActionEvent event) {
        bar.setProgress(bar.getProgress() - 0.1);        
        actualizarIndicator();
    }
    
    public void actualizarIndicator(){
        indicator.setProgress(bar.getProgress());
    }
    
}
