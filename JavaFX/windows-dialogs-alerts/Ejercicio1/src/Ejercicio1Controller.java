/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio1Controller implements Initializable {

    @FXML
    private Button none;
    @FXML
    private Button wm;
    @FXML
    private Button am;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void openNone(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.NONE);
        stage.show();
    }

    @FXML
    private void openWindowModal(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
    }

    @FXML
    private void openApplicationModal(ActionEvent event) {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.show();
    }
    
}
