/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio5Controller implements Initializable {

    @FXML
    private Label label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirm(ActionEvent event) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        
        ButtonType button1 = new ButtonType("Hola");
        ButtonType button2 = new ButtonType("Casa");
        ButtonType button3 = new ButtonType("Perro");
        alert.getButtonTypes().setAll(button1, button2, button3);
        
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == button1)
            label.setText(button1.getText());
        else if(result.get() == button2)
            label.setText(button2.getText());
        else
            label.setText(button3.getText());
    }
}
