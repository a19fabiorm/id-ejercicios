/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio3Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void showAlert(ActionEvent event) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setHeaderText("Fichero no encontrado");
        alert.setContentText("El fichero indicado no existe!");
        
        Label label = new Label("Mi etiqueta");
        TextArea textArea = new TextArea("Área de texto");
        
        FlowPane pane = new FlowPane();
        pane.getChildren().addAll(label, textArea);
        
        alert.getDialogPane().setExpandableContent(pane);
        alert.showAndWait();
    }
    
}
