/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio8Controller implements Initializable {
    
    Alert dialog = new Alert(AlertType.CONFIRMATION);
    Parent content;
    ButtonType login;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dialog.setTitle("Login");
        dialog.setHeaderText("Diálogo personalizado");
        login = new ButtonType("Login", ButtonData.OK_DONE);
        dialog.getButtonTypes().set(0, login);
        dialog.getDialogPane().lookupButton(login).setDisable(true);
        
        try {
            content = FXMLLoader.load(getClass().getResource("content.fxml"));
            dialog.getDialogPane().setContent(content);
            ImageView image = new ImageView("enter.png");
            image.setFitHeight(48);
            image.setFitWidth(48);
            dialog.getDialogPane().setGraphic(image);
            
            FilteredList<Node> list = content.getChildrenUnmodifiable().filtered((t) -> {
                return t.getClass() == TextField.class;
            });
            
            TextField user = (TextField) list.get(0);
            
            user.addEventHandler(EventType.ROOT, (t) -> {
                if(user.getText().isBlank())
                    dialog.getDialogPane().lookupButton(login).setDisable(true);
                else
                    dialog.getDialogPane().lookupButton(login).setDisable(false);
            });
        } catch (IOException ex) {
            Logger.getLogger(Ejercicio8Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void login(ActionEvent event) {
        Optional<ButtonType> result = dialog.showAndWait();
        if(result.get() == login){
            FilteredList<Node> list = content.getChildrenUnmodifiable().filtered((t) -> {
                return t.getClass() == TextField.class || t.getClass() == PasswordField.class;
            });
            
            String mensaje = "";
            for(Node node : list){
                if(node.getClass() == TextField.class){
                    mensaje += "usuario: " + ((TextField)node).getText() + "\n";
                }else if(node.getClass() == PasswordField.class){
                    mensaje += "contrasinal: " + ((PasswordField)node).getText() + "\n";
                }
            }
            System.out.println(mensaje);
        }
    }
    
}
