/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author ipman
 */
public class Ejercicio2Controller implements Initializable {

    @FXML
    private Button personalizado;
    @FXML
    private Button cabecera;
    @FXML
    private Button sincabecera;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void personalizado(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Cabecera personalizada");
        alert.setContentText("Contenido personalizado");
        alert.show();
    }

    @FXML
    private void cabecera(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.show();
    }
    
    @FXML
    private void sincabecera(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.show();
    }
    
}
