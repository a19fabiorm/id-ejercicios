package GestionClientes.logica;

import GestionClientes.dto.Cliente;
import java.util.ArrayList;

/**
 *
 * @author a19danielvt
 */
public class LogicaNegocio {
    private static ArrayList<Cliente> clientes = new ArrayList<>();
    
    public static void AñadirCliente(Cliente c){
        clientes.add(c);
    }

    public static ArrayList<Cliente> getClientes() {
        return clientes;
    }
}
